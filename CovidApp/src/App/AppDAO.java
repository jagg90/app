package App;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class AppDAO {

    SessionFactory factory;
    Session session = null;

    public AppDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public List<covidData> getCovidData(String ctry, String prov, String date1, String date2) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            //Query Data from our Database based on user parameters

            String sql = "from App.covidData where country_region=" + '\'' + ctry + '\'' + "and province_state=" + '\'' + prov + '\'' + "and date between" + '\'' + date1 + '\'' + "and" + '\'' + date2 + '\'';
            List<covidData> st = (List<covidData>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return st;

        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }


}













