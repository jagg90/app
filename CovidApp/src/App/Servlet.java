package App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        try {

            // Get data from submission
            String getCountryID = req.getParameter("country");
            String getProvID = req.getParameter("state");
            String startDate = req.getParameter("startDate");
            String lastDate = req.getParameter("lastDate");

            PrintWriter out = resp.getWriter();

            // Begin the output of the data

            resp.setContentType("text/html");
            out.println("<html><head></head><body>");
            out.println("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>");
            out.println("<title>Covid Cases Summary</title>");

            // Get the JSON class to identify the ID for country and region

            json j = new json();

            String country = j.getLocationIDS(getCountryID);

            String province = j.getLocationIDS(getProvID);


            out.println("<h1 class='container mt-5'>Covid Cases Tracker:</h1>");

            out.println("</br>");

            out.println("<h3>"  + " Cases for "+ province + ", " + country + " " + " From "+ startDate + " to " + lastDate + " " + "</h3>");


            //Get AppDAO class to get data from Database based on the user parameters

            AppDAO t = new AppDAO();

            List<covidData> s = t.getCovidData(country, province, startDate, lastDate);

            out.println("</br>");

            //Display Information

            for (covidData i : s) out.println("<div class='pb-4'>" + i + "</div>");

            out.println("</table>");


            out.println("<br/>");
            out.println("<div class='container mt-5'>" + "<div class='mb-4'>" + "<a href='http://localhost:8080/CovidApp_war_exploded/' >Return to the home page</a>"
                            + "</div>" + "<div class='row'>" + "<div class='col-sm-12'>");

            out.println("<br><p>Content source:</p>");
            out.println("<footer><a href=\"https://coronavirus.jhu.edu/\">\n" +
                    "    <img src=\"https://coronavirus.jhu.edu/images/network/7e307f8f5ea94f90b172bf0046167993.png\"\n" +
                    "</a></footer>");


            out.println("</body></html>");

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

}

