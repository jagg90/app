package App;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "covid19_data")

public class covidData{


    @Id
    @Column(name = "row")
    int row;
    @Column(name = "province_state")
    String province_state;
    @Column(name = "country_region")
    String country_region;
    @Column(name = "date")
    String date;
    @Column(name = "total_confirmed")
    String total_confirmed;
    @Column(name = "total_recovered")
    String total_recovered;
    @Column(name = "total_deaths")
    String total_deaths;
    @Column(name = "new_cases")
    String new_cases;
    @Column(name = "recovered")
    String recovered;
    @Column(name = "deaths")
    String deaths;

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getCountry_region() {
        return country_region;
    }

    public void setCountry_region(String country_region) {
        this.country_region = country_region;
    }

    public String getProvince_state() {
        return province_state;
    }

    public void setProvince_state(String province_state) {
        this.province_state = province_state;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotal_confirmed() {
        return total_confirmed;
    }

    public void setTotal_confirmed(String total_confirmed) {
        this.total_confirmed = total_confirmed;
    }

    public String getTotal_recovered() {
        return total_recovered;
    }

    public void setTotal_recovered(String total_recovered) {
        this.total_recovered = total_recovered;
    }

    public String getTotal_deaths() {
        return total_deaths;
    }

    public void setTotal_deaths(String total_deaths) {
        this.total_deaths = total_deaths;
    }

    public String getRecovered() {
        return recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

    public String getDeaths() {
        return deaths;
    }

    public void setDeaths(String deaths) {
        this.deaths = deaths;
    }

    public String getNew_cases() {
        return new_cases;
    }

    public void setNew_cases(String new_cases) {
        this.new_cases = new_cases;
    }


//        return " | " + " | " + " Date " +  date + " | " + " " +" | " + " " +" Confirmed "+ new_cases+ " "+" | " +" | " + " " + deaths + " Deaths "+ " | " +" | " + " " +recovered + " Recovered "+" | " + " | " + " ";


    @Override
    public String
    toString() {
        return  " - " + " | " + " " +" | " + " " + "date=" + date + " | " + " " +" | " + " " +
                "New Cases=" + new_cases + " | " + " " +" | " + " " +
                "New People recovered=" + recovered + " | " + " " +" | " + " " +
                "Daily deaths=" + deaths + " | " + " " +" | " + " " +
                "total_confirmed=" + total_confirmed + " | " + " " +" | " + " " +
                "total_deaths=" + total_deaths + " | " + " " +" | " + " " +
                "total_recovered=" + total_recovered + " | " + " " +" | " + " " + " - ";
    }
}
