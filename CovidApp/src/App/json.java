package App;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class json {


    // Get JSON data and put it on an array

    String idJSON;

    {
        try {
            idJSON = FileUtils.readFileToString(new File("/Users/jagg90/git/app/CovidApp/src/App/id_list.json"), String.valueOf(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    locationID[] locationIDS = new Gson().fromJson(idJSON, locationID[].class);

    public String getLocationIDS(String id) {

        //Based on ID get a specific element from the JSON array

        try {
            for (locationID locationID : locationIDS) {

                if (locationID.getId().equals(id)) {

                    return locationID.getName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }
}











