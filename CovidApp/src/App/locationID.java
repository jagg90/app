package App;

public class locationID {

    private String name;
    private String id;
    private int parent;

    public locationID(String name, String id, int parent){

        this.name = name;
        this.id = id;
        this.parent = parent;

    }


    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public int getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", parent='" + parent + '\'' +
                '}';
    }
}
