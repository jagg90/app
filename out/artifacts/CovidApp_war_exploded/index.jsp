<%--
  Created by IntelliJ IDEA.
  User: jagg90
  Date: 7/7/20
  Time: 3:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>CovidApp</title>
  <link rel="stylesheet" href="css/app.css" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="banner">
  <img class="banner-image" src="world-map.png">
</div>


<form class="main-form" action="Servlet" method="get">


  <h1 class="main-title">Covid 19 Data</h1>


  <select name="country" id="country" class="form-control input-lg" required="required">
    <option value="">Select country</option>
  </select>

  <br /><br />

  <select name="state" id="state" class="form-control input-lg" required="required">
    <option value="">Select a Province/State:</option>
  </select>

  <br/><br/>

  <label for="startDate">Start date:</label>

  <input type="date" id="startDate" name="startDate" min="2020-01-01" max="2020-07-06" required="required"/>
  <br/><br/>

  <label for="lastDate">End date:</label>

  <input type="date" id="lastDate" name="lastDate" min="2020-01-01" max="2020-07-06" required="required">

  <p><input type="submit" value="Select" /></p>

</form>

<img class="cases-image" src="worldcases.png">

<footer class="footer">
  <p> For more information visit the link below </p>
  <a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">World Health Organization</a></p>


</footer>


</body>
</html>


<script>
  $(document).ready(function(){

    load_json_data('country');

    function load_json_data(id, parent_id)
    {
      var html_code = '';
      $.getJSON('country_province.json', function(data){

        html_code += '<option value="">Select '+id+'</option>';
        $.each(data, function(key, value){
          if(id == 'country')
          {
            if(value.parent_id == '0')
            {
              html_code += '<option value="'+value.id+'">'+value.name+'</option>';
            }
          }
          else
          {
            if(value.parent_id == parent_id)
            {
              html_code += '<option value="'+value.id+'">'+value.name+'</option>';
            }
          }
        });
        $('#'+id).html(html_code);
      });

    }

    $(document).on('change', '#country', function(){
      var country_id = $(this).val();
      if(country_id != '')
      {
        load_json_data('state', country_id);
      }
      else
      {
        $('#state').html('<option value="">Select state</option>');

      }
    });
  });
</script>

